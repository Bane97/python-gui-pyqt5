import sys
from ui import *
from PyQt5.QtWidgets import QDial, QApplication, QInputDialog, QListWidgetItem

class MyForm(QDial):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.addButton.clicked.connect(self.add)
        self.ui.deleteButton.clicked.connect(self.delete)
        self.ui.editButton.clicked.connect(self.edit)



    def delete(self):
        self.ui.listWidget.takeItem(self.ui.listWidget.currentRow())

    def add(self):
        a = self.ui.lineEdit.text()
        if len(a)!=0:
            self.ui.listWidget.addItem(a)

    def edit(self):
        row = self.ui.listWidget.currentRow()
        text, response = QInputDialog.getText(self,'Unos teksta', 'Unesi tekts')
        if response:
            self.ui.listWidget.takeItem(row)
            self.ui.listWidget.insertItem(row, QListWidgetItem(text))

if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()

    sys.exit(app.exec_())
