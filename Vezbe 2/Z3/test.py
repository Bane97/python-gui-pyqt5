#Spinbox
import sys
from ui import *  # pylint: disable=E0611
from PyQt5.QtWidgets import QDial, QApplication # pylint: disable=E0611




class MyForm(QDial):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.doubleSpinBox.valueChanged.connect(self.res)

    def res(self):
        if(len(self.ui.lineEdit.text())!=0):
            a = self.ui.doubleSpinBox.value()
            b = float(self.ui.lineEdit.text())
            self.ui.label_2.setText(str(round(a*b, 2)))

        




if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()

    sys.exit(app.exec_())