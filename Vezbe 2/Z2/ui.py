# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'untitled.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        self.num1LineEdit = QtWidgets.QLineEdit(Dialog)
        self.num1LineEdit.setGeometry(QtCore.QRect(130, 60, 113, 20))
        self.num1LineEdit.setObjectName("num1LineEdit")
        self.num2LineEdit = QtWidgets.QLineEdit(Dialog)
        self.num2LineEdit.setGeometry(QtCore.QRect(130, 90, 113, 20))
        self.num2LineEdit.setObjectName("lineEdit_2")
        self.addButton = QtWidgets.QPushButton(Dialog)
        self.addButton.setGeometry(QtCore.QRect(30, 170, 61, 51))
        self.addButton.setObjectName("addButton")
        self.subButton = QtWidgets.QPushButton(Dialog)
        self.subButton.setGeometry(QtCore.QRect(120, 170, 61, 51))
        self.subButton.setObjectName("subButton")
        self.devButton = QtWidgets.QPushButton(Dialog)
        self.devButton.setGeometry(QtCore.QRect(300, 170, 61, 51))
        self.devButton.setObjectName("devButton")
        self.mulButton = QtWidgets.QPushButton(Dialog)
        self.mulButton.setGeometry(QtCore.QRect(210, 170, 61, 51))
        self.mulButton.setObjectName("mulButton")
        self.res = QtWidgets.QLabel(Dialog)
        self.res.setGeometry(QtCore.QRect(110, 250, 161, 21))
        self.res.setText("")
        self.res.setObjectName("res")
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(60, 60, 47, 13))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(Dialog)
        self.label_3.setGeometry(QtCore.QRect(56, 90, 61, 16))
        self.label_3.setObjectName("label_3")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.addButton.setText(_translate("Dialog", "+"))
        self.subButton.setText(_translate("Dialog", "-"))
        self.devButton.setText(_translate("Dialog", "/"))
        self.mulButton.setText(_translate("Dialog", "*"))
        self.label_2.setText(_translate("Dialog", "Prvi broj"))
        self.label_3.setText(_translate("Dialog", "Drugi broj"))
