import sys
from ui import *  # pylint: disable=E0611
from PyQt5.QtWidgets import QDial, QApplication # pylint: disable=E0611


#provera da li je float
def isFloat(a):
    try:
        float(a)
        return True
    except:
        return False


class MyForm(QDial):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.addButton.clicked.connect(self.addTwoNum)
        self.ui.subButton.clicked.connect(self.subTwoNum)
        self.ui.mulButton.clicked.connect(self.mulTwoNum)
        self.ui.devButton.clicked.connect(self.divTwoNum)
        

    def addTwoNum(self):
        flag = False
        if(isFloat(self.ui.num1LineEdit.text())):
            a = float(self.ui.num1LineEdit.text())
        else:
            flag = True
            self.ui.num1LineEdit.setText("Nije pravilno unet broj")
        if(isFloat(self.ui.num2LineEdit.text())):
            b = float(self.ui.num2LineEdit.text())
        else:
            self.ui.num2LineEdit.setText("Nije pravilno unet broj")
            flag = True
        if(flag):
            return
        self.ui.res.setText(str(round(a+b, 2)))


    def subTwoNum(self):
        flag = False
        if(isFloat(self.ui.num1LineEdit.text())):
            a = float(self.ui.num1LineEdit.text())
        else:
            flag = True
            self.ui.num1LineEdit.setText("Nije pravilno unet broj")
        if(isFloat(self.ui.num2LineEdit.text())):
            b = float(self.ui.num2LineEdit.text())
        else:
            self.ui.num2LineEdit.setText("Nije pravilno unet broj")
            flag = True
        if(flag):
            return
        self.ui.res.setText(str(round(a-b, 2)))
    
    def mulTwoNum(self):
        flag = False
        if(isFloat(self.ui.num1LineEdit.text())):
            a = float(self.ui.num1LineEdit.text())
        else:
            flag = True
            self.ui.num1LineEdit.setText("Nije pravilno unet broj")
        if(isFloat(self.ui.num2LineEdit.text())):
            b = float(self.ui.num2LineEdit.text())
        else:
            self.ui.num2LineEdit.setText("Nije pravilno unet broj")
            flag = True
        if(flag):
            return
        self.ui.res.setText(str(round(a*b, 2)))
        
    def divTwoNum(self):
        flag = False
        if(isFloat(self.ui.num1LineEdit.text())):
            a = float(self.ui.num1LineEdit.text())
        else:
            flag = True
            self.ui.num1LineEdit.setText("Nije pravilno unet broj")
        if(isFloat(self.ui.num2LineEdit.text())):
            b = float(self.ui.num2LineEdit.text())
        else:
            self.ui.num2LineEdit.setText("Nije pravilno unet broj")
            flag = True
        if(flag):
            return
        self.ui.res.setText(str(round(a/b, 2)))




if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()

    sys.exit(app.exec_())
