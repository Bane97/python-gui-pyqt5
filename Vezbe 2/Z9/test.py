import sys
from ui import *
from PyQt5.QtWidgets import QDial, QApplication

class MyForm(QDial):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        myFont = QtGui.QFont(self.ui.fontComboBox.currentText(), 12)
        self.ui.textEdit.setFont(myFont)
        self.ui.fontComboBox.currentFontChanged.connect(self.res)

    
    def res(self):
        myFont = QtGui.QFont(self.ui.fontComboBox.currentText(), 12)
        self.ui.textEdit.setFont(myFont)
        
        




if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()

    sys.exit(app.exec_())
