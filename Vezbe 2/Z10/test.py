import sys
from ui import *
from PyQt5.QtWidgets import QDial, QApplication

class MyForm(QDial):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.pushButton.clicked.connect(self.res)

    def res(self):
        i = 0
        while i<100:
            self.ui.progressBar.setValue(i)
            i += 0.0001


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()

    sys.exit(app.exec_())
