#WidgetItems
import sys
from ui import *
from PyQt5.QtWidgets import QDialog, QApplication

class MyForm(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.listWidget.itemActivated.connect(self.res)

    def res(self):
        #print(self.ui.listWidget.currentItem()
        #print(self.ui.listWidget.currentItem().text())
        self.ui.label.setText(self.ui.listWidget.currentItem().text())

if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()

    sys.exit(app.exec_())
