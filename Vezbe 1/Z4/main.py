import sys
from PyQt5.QtWidgets import QApplication, QDialog
from design import *


class MyForm(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.scorpionCheckBox.stateChanged.connect(self.price)
        self.ui.guacamoleCheckBox.stateChanged.connect(self.price)
        self.ui.nacosCheckBox.stateChanged.connect(self.price)

    def price(self):
        amount = 7
        if self.ui.scorpionCheckBox.isChecked():
            amount = amount + 2
        if self.ui.nacosCheckBox.isChecked():
            amount = amount + 1
        if self.ui.guacamoleCheckBox.isChecked():
            amount = amount + 4

        self.ui.outputLabel.setText("Cena je "+str(amount)+ " $.")

if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
