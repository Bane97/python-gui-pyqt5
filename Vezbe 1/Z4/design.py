# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'untitled.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(40, 50, 47, 13))
        self.label.setObjectName("label")
        self.scorpionCheckBox = QtWidgets.QCheckBox(Dialog)
        self.scorpionCheckBox.setGeometry(QtCore.QRect(40, 80, 101, 17))
        self.scorpionCheckBox.setObjectName("scorpionCheckBox")
        self.nacosCheckBox = QtWidgets.QCheckBox(Dialog)
        self.nacosCheckBox.setGeometry(QtCore.QRect(40, 140, 91, 17))
        self.nacosCheckBox.setObjectName("nacosCheckBox")
        self.guacamoleCheckBox = QtWidgets.QCheckBox(Dialog)
        self.guacamoleCheckBox.setGeometry(QtCore.QRect(40, 110, 70, 17))
        self.guacamoleCheckBox.setObjectName("guacamoleCheckBox")
        self.outputLabel = QtWidgets.QLabel(Dialog)
        self.outputLabel.setGeometry(QtCore.QRect(60, 220, 221, 16))
        self.outputLabel.setText("")
        self.outputLabel.setObjectName("outputLabel")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label.setText(_translate("Dialog", "Burito 7$"))
        self.scorpionCheckBox.setText(_translate("Dialog", "Skorpion Sos 2$"))
        self.nacosCheckBox.setText(_translate("Dialog", "Guacamole 4$"))
        self.guacamoleCheckBox.setText(_translate("Dialog", "Nacos 1$"))
