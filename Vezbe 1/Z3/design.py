# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'untitled.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(110, 200, 161, 16))
        self.label.setText("")
        self.label.setObjectName("label")
        self.verticalLayoutWidget = QtWidgets.QWidget(Dialog)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(20, 60, 71, 111))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.samsungRadioButton = QtWidgets.QRadioButton(self.verticalLayoutWidget)
        self.samsungRadioButton.setObjectName("samsungRadioButton")
        self.verticalLayout.addWidget(self.samsungRadioButton)
        self.iPhoneRadioButton = QtWidgets.QRadioButton(self.verticalLayoutWidget)
        self.iPhoneRadioButton.setObjectName("iPhoneRadioButton")
        self.verticalLayout.addWidget(self.iPhoneRadioButton)
        self.xiaomiRadioButton = QtWidgets.QRadioButton(self.verticalLayoutWidget)
        self.xiaomiRadioButton.setObjectName("xiaomiRadioButton")
        self.verticalLayout.addWidget(self.xiaomiRadioButton)
        self.verticalLayoutWidget_2 = QtWidgets.QWidget(Dialog)
        self.verticalLayoutWidget_2.setGeometry(QtCore.QRect(210, 60, 71, 111))
        self.verticalLayoutWidget_2.setObjectName("verticalLayoutWidget_2")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.eightRadioButton = QtWidgets.QRadioButton(self.verticalLayoutWidget_2)
        self.eightRadioButton.setObjectName("eightRadioButton")
        self.verticalLayout_2.addWidget(self.eightRadioButton)
        self.fourRadioButton = QtWidgets.QRadioButton(self.verticalLayoutWidget_2)
        self.fourRadioButton.setObjectName("fourRadioButton")
        self.verticalLayout_2.addWidget(self.fourRadioButton)
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(20, 30, 81, 16))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(Dialog)
        self.label_3.setGeometry(QtCore.QRect(210, 30, 81, 16))
        self.label_3.setObjectName("label_3")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.samsungRadioButton.setText(_translate("Dialog", "Samsung"))
        self.iPhoneRadioButton.setText(_translate("Dialog", "iPhone"))
        self.xiaomiRadioButton.setText(_translate("Dialog", "Xiaomi"))
        self.eightRadioButton.setText(_translate("Dialog", "4 GB"))
        self.fourRadioButton.setText(_translate("Dialog", "8 GB"))
        self.label_2.setText(_translate("Dialog", "Odaberite model"))
        self.label_3.setText(_translate("Dialog", "RAM"))
