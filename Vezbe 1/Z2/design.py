# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'untitled.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        self.samsungRadioButton = QtWidgets.QRadioButton(Dialog)
        self.samsungRadioButton.setGeometry(QtCore.QRect(60, 80, 82, 17))
        self.samsungRadioButton.setObjectName("samsungRadioButton")
        self.iPhoneRadioButton = QtWidgets.QRadioButton(Dialog)
        self.iPhoneRadioButton.setGeometry(QtCore.QRect(60, 110, 82, 17))
        self.iPhoneRadioButton.setObjectName("iPhoneRadioButton")
        self.xiaomiRadioButton = QtWidgets.QRadioButton(Dialog)
        self.xiaomiRadioButton.setGeometry(QtCore.QRect(60, 140, 82, 17))
        self.xiaomiRadioButton.setObjectName("xiaomiRadioButton")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(110, 200, 161, 16))
        self.label.setText("")
        self.label.setObjectName("label")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.samsungRadioButton.setText(_translate("Dialog", "Samsung"))
        self.iPhoneRadioButton.setText(_translate("Dialog", "iPhone"))
        self.xiaomiRadioButton.setText(_translate("Dialog", "Xiaomi"))
