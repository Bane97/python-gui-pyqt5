import sys
from PyQt5.QtWidgets import QApplication, QDialog
from design import *


class MyForm(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        #dodajemo siglan za svaki button
        self.ui.samsungRadioButton.clicked.connect(self.dispaly)
        self.ui.iPhoneRadioButton.clicked.connect(self.dispaly)
        self.ui.xiaomiRadioButton.clicked.connect(self.dispaly)


    def dispaly(self):
        if self.ui.samsungRadioButton.isChecked():
            cena = 600
        if self.ui.iPhoneRadioButton.isChecked():
            cena = 1100
        if self.ui.xiaomiRadioButton.isChecked():
            cena = 250
        
        self.ui.label.setText("Cena je "+ str(cena)+ "$.")



if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
