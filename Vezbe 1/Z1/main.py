import sys
from PyQt5.QtWidgets import QDialog, QApplication
from design import *

#Klasa myForm nasledjuje klasu QDialog
class MyForm(QDialog):
    def __init__(self):
        super().__init__() #kreirali smo objekat
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.helloButton.clicked.connect(self.hello)

    #bice poznava kao self.hello pa zato prima parametar self
    def hello(self):
        self.ui.outputLabel.setText("Hello "+ self.ui.insertLineEdit.text())

if __name__ =='__main__':
    app = QtWidgets.QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())


