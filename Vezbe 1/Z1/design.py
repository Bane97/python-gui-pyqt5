# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'untitled.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 300)
        self.nameLabel = QtWidgets.QLabel(Dialog)
        self.nameLabel.setGeometry(QtCore.QRect(80, 90, 91, 16))
        self.nameLabel.setObjectName("nameLabel")
        self.insertLineEdit = QtWidgets.QLineEdit(Dialog)
        self.insertLineEdit.setGeometry(QtCore.QRect(170, 90, 113, 20))
        self.insertLineEdit.setObjectName("insertLineEdit")
        self.helloButton = QtWidgets.QPushButton(Dialog)
        self.helloButton.setGeometry(QtCore.QRect(190, 140, 75, 23))
        self.helloButton.setObjectName("helloButton_2")
        self.outputLabel = QtWidgets.QLabel(Dialog)
        self.outputLabel.setGeometry(QtCore.QRect(80, 250, 271, 20))
        self.outputLabel.setText("")
        self.outputLabel.setObjectName("outputLabel")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.nameLabel.setText(_translate("Dialog", "Unesite vase ime:"))
        self.helloButton.setText(_translate("Dialog", "Pozdrav"))
