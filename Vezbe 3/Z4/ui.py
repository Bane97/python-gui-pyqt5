# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'untitled.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(766, 561)
        self.calendarWidget = QtWidgets.QCalendarWidget(Dialog)
        self.calendarWidget.setGeometry(QtCore.QRect(180, 90, 312, 183))
        self.calendarWidget.setObjectName("calendarWidget")
        self.comboBox = QtWidgets.QComboBox(Dialog)
        self.comboBox.setGeometry(QtCore.QRect(300, 280, 69, 22))
        self.comboBox.setObjectName("comboBox")
        self.spinBox = QtWidgets.QSpinBox(Dialog)
        self.spinBox.setGeometry(QtCore.QRect(310, 310, 42, 22))
        self.spinBox.setObjectName("spinBox")
        self.pushButton = QtWidgets.QPushButton(Dialog)
        self.pushButton.setGeometry(QtCore.QRect(300, 340, 75, 23))
        self.pushButton.setObjectName("pushButton")
        self.res1Label = QtWidgets.QLabel(Dialog)
        self.res1Label.setGeometry(QtCore.QRect(110, 430, 581, 21))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.res1Label.setFont(font)
        self.res1Label.setText("")
        self.res1Label.setObjectName("res1Label")
        self.label_3 = QtWidgets.QLabel(Dialog)
        self.label_3.setGeometry(QtCore.QRect(150, 40, 401, 31))
        font = QtGui.QFont()
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.res2Label = QtWidgets.QLabel(Dialog)
        self.res2Label.setGeometry(QtCore.QRect(110, 470, 581, 21))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.res2Label.setFont(font)
        self.res2Label.setText("")
        self.res2Label.setObjectName("res2Label")
        self.label_4 = QtWidgets.QLabel(Dialog)
        self.label_4.setGeometry(QtCore.QRect(220, 280, 61, 16))
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(Dialog)
        self.label_5.setGeometry(QtCore.QRect(230, 310, 61, 16))
        self.label_5.setObjectName("label_5")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.pushButton.setText(_translate("Dialog", "Rezervisi"))
        self.label_3.setText(_translate("Dialog", "REZERVACIJA HOTELSKOG SMESTAJA"))
        self.label_4.setText(_translate("Dialog", "Odabir sobe"))
        self.label_5.setText(_translate("Dialog", "Broj osoba"))
