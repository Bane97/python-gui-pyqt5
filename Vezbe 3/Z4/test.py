#App za rezervaciju soba

import sys
from PyQt5.QtWidgets import QDialog, QApplication
from ui import *






class MyForm(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.roomTypes = ['prva','druga','treca']
        self.addData()
        self.ui.pushButton.clicked.connect(self.book)

    
    def addData(self):
        for i in self.roomTypes:
            self.ui.comboBox.addItem(i)

    def book(self):
        date = self.ui.calendarWidget.selectedDate().toString('dd.MM.yyyy.')
        noPer = self.ui.spinBox.value()
        roomType = self.ui.comboBox.currentText()
        self.ui.res1Label.setText("Odabrana je "+roomType+" soba, za "+str(noPer)+" osobe, od "+ date)
        price = 0
        if(roomType == 'prva'):
            price = 10
        if(roomType == 'druga'):
            price = 20
        if(roomType == 'treca'):
            price = 30
        price *=noPer

        self.ui.res2Label.setText('Ukupan iznos za naplatu je '+str(price)+' \u20ac.')
    

if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
