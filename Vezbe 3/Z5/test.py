#Objektno orjentisano
#Klase
import sys
from PyQt5.QtWidgets import QDialog, QApplication
from ui import *


#Preko klase
class Student:
    name = ''
    index = ''
    def __init__(self, index, name):
        self.index = index
        self.name = name
    def predstaviSe(self):
        return(self.name + '  '+ self.index)


class MyForm(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.pushButton.clicked.connect(self.res)
    
    

    def res(self):
        name = self.ui.lineEdit.text()
        index = self.ui.indexLineEdit.text()
        self.ui.resLineEdit.setText(name + '  '+ index)
        student = Student(name, index)
        self.ui.resLineEdit.setText(student.predstaviSe())
      

   

if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
