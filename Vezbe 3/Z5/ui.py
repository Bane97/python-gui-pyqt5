# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'untitled.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 273)
        self.lineEdit = QtWidgets.QLineEdit(Dialog)
        self.lineEdit.setGeometry(QtCore.QRect(80, 30, 201, 20))
        self.lineEdit.setObjectName("lineEdit")
        self.indexLineEdit = QtWidgets.QLineEdit(Dialog)
        self.indexLineEdit.setGeometry(QtCore.QRect(80, 90, 201, 20))
        self.indexLineEdit.setObjectName("indexLineEdit")
        self.pushButton = QtWidgets.QPushButton(Dialog)
        self.pushButton.setGeometry(QtCore.QRect(140, 200, 75, 23))
        self.pushButton.setObjectName("pushButton")
        self.resLineEdit = QtWidgets.QLineEdit(Dialog)
        self.resLineEdit.setEnabled(False)
        self.resLineEdit.setGeometry(QtCore.QRect(80, 150, 201, 20))
        self.resLineEdit.setObjectName("resLineEdit")
        self.nameLabel = QtWidgets.QLabel(Dialog)
        self.nameLabel.setGeometry(QtCore.QRect(80, 10, 47, 13))
        self.nameLabel.setObjectName("nameLabel")
        self.indexLabel = QtWidgets.QLabel(Dialog)
        self.indexLabel.setGeometry(QtCore.QRect(80, 70, 47, 13))
        self.indexLabel.setObjectName("indexLabel")
        self.resLabel = QtWidgets.QLabel(Dialog)
        self.resLabel.setGeometry(QtCore.QRect(80, 130, 47, 13))
        self.resLabel.setObjectName("resLabel")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.pushButton.setText(_translate("Dialog", "Klikni me!"))
        self.nameLabel.setText(_translate("Dialog", "Ime"))
        self.indexLabel.setText(_translate("Dialog", "Index"))
        self.resLabel.setText(_translate("Dialog", "Rezultat"))
