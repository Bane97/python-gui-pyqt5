#LCD NUmber

import sys
from PyQt5.QtWidgets import QDialog, QApplication
from ui import *

#Domaci: Napraviti stopericu




class MyForm(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.cnt = 0
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.res)
        timer.start(1000)

    def res(self):
        self.cnt += 1
        self.ui.lcdNumber.display(str(self.cnt))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
