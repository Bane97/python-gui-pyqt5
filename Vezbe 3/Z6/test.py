#Klase
class Student:
    name = None
    index = None
    def __init__(self, index, name):
        #Enkapsulacija
        #self.__index = index  (privatno)
        #self.index = index
        self.index = index
        self.name = name
    def getIndex(self):
        return self.index
    def getName(self):
        return self.name

#class Marks(Student): Marsk nasledjuje Student
class Marks(Student):
    k1 = 0
    k2 = 0
    def __init__(self, index, name, k1, k2):
        #.__init__ pozivanje konstruktora 
        #super().__init__ ista stvar (super poziva roditeljku klasu)
        Student.__init__(self, index, name)
        self.k1 = k1
        self.k2 = k2

s = Marks("EM$","Pera",20,40)
print(s.k1)
print(s.name)


