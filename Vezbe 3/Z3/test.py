#LCD NUmber
#Sat
#Trenutno vreme

import sys
from PyQt5.QtWidgets import QDialog, QApplication
from ui import *

#Domaci: Napraviti stopericu




class MyForm(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.calendarWidget.selectionChanged.connect(self.res)
        self.ui.dateEdit.setDate(self.ui.calendarWidget.selectedDate())
        self.ui.dateEdit.setDisplayFormat('dd/MM/yyyy')

    def res(self):
        self.ui.dateEdit.setDate(self.ui.calendarWidget.selectedDate())
    

if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
