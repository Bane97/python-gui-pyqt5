#Igranje sa fontovima
import sys
from PyQt5.QtWidgets import QDialog, QApplication, QFontDialog
from ui import *


class MyForm(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.pushButton.clicked.connect(self.hello)

    
    def hello(self):
        font, status = QFontDialog.getFont()
        if status:
            self.ui.textEdit.setFont(font)



if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
