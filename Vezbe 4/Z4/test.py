#Igranje sa fontovima
import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QFileDialog
from ui import *


class MyForm(QMainWindow):
    def __init__(self):
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.actionOpen.triggered.connect(self.openDialog)
        self.ui.actionSave.triggered.connect(self.saveDialog)


    
    def openDialog(self):
        path, fil = QFileDialog.getOpenFileName(self,'Open file', '', 'All Files (*);;Text Files (*.txt);;Python (*.py)')
        if path:
            f = open(path,'r') #Otvaramo i citamo fajl
            data = f.read()
            self.ui.textEdit.setText(data)
            f.close()


    def saveDialog(self):
        path, fil = QFileDialog.getSaveFileName(self,'Save file', '', 'All Files (*);;Text Files (*.txt);;Python (*.py)')
        if path:
            text = self.ui.textEdit.toPlainText()
            f = open(path,'w')
            f.write(text)
            f.close
        
  


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
