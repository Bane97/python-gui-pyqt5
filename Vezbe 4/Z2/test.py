#Igranje sa bojama
import sys
from PyQt5.QtWidgets import QDialog, QApplication, QColorDialog
from ui import *
from PyQt5.QtGui import QColor

class MyForm(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        """
        col = QColor(123,251,6)
        self.ui.frame.setStyleSheet("background-color: "+col.name())
        """

        self.ui.pushButton.clicked.connect(self.hello)


    def hello(self):
        col = QColorDialog.getColor()
        if col.isValid(): #Provera da li je boja u redu
            self.ui.frame.setStyleSheet("background-color: "+col.name())
            self.ui.pushButton.setStyleSheet("background-color: "+col.name())



if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
