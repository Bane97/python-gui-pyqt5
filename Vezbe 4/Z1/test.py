import sys
from PyQt5.QtWidgets import QDialog, QApplication, QInputDialog
from ui import *

class MyForm(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.pushButton.clicked.connect(self.hello)

    def hello(self):
        #response, status = QInputDialog.getInt(self,"Spin Box", "Odaberite kolicinu",0,0,100,1)
        #response, status = QInputDialog.getText(self,"Spin Box", "Odaberite kolicinu")
        drzave = ['Antigva i Barbuda', 'Barbados', 'Srbija','Trinidad i Tobago']
        response, status = QInputDialog.getItem(self,"Spin Box", "Odaberite drzavu",drzave,0,False)

        if status:
            self.ui.label.setText(str(response))




if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MyForm()
    w.show()
    sys.exit(app.exec_())
